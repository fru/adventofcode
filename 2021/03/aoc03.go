package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getDirectionAndValue(line string) (string, int) {
    splits := strings.Split(line, " ")
    value, err := strconv.Atoi(splits[1])
    checkError(err)
    return splits[0], value
}

func getMostCommon(values []string) string {
    counts := make([]int, len(values[0]))

    for _, value := range values {
        for i, char := range value {
            n, err := strconv.Atoi(string(char))
            checkError(err)
            counts[i] += n
        }
    }
    chars := make([]string, len(counts))
    half := float64(len(values)) / 2
    for i, count := range counts {
        if float64(count) < half {
            chars[i] = "0"
        } else {
            chars[i] = "1"
        }
    }
    return strings.Join(chars, "")
}

func getLeastCommon(values []string) string {
    mostCommon := getMostCommon(values)
    leastCommon := ""
    for _, char := range mostCommon {
        if char == '0' {
            leastCommon += "1"
        } else {
            leastCommon += "0"
        }
    }
    return leastCommon
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

type commonFunc func([]string) string

func getEntry(lines []string, commonFn commonFunc) string {
    entries := make([]string, len(lines))
    copy(entries, lines)
    pos := 0
    
    for len(entries) > 1 {
        common := commonFn(entries)
        tmp := make([]string, 0)
        for _, entry := range entries {
            if entry[pos] == common[pos] {
                tmp = append(tmp, entry)
            }
        }
        entries = tmp
        pos++
    }
    // fmt.Printf("getEntry: %s => %d\n", entries[0], binToDec(entries[0]))
    return entries[0]
}

func binToDec(binary string) int64 {
    value, err := strconv.ParseInt(binary, 2, 64)
    checkError(err)
    return value
}

func main() {
    lines := getLinesFromFile("input.txt")
    mostCommon := getMostCommon(lines)
    leastCommon := getLeastCommon(lines)
    fmt.Printf("mostCommon: %s => %d\n", mostCommon, binToDec(mostCommon))
    fmt.Printf("leastCommon: %s => %d\n", leastCommon, binToDec(leastCommon))
    a := binToDec(mostCommon)
    b := binToDec(leastCommon)
    fmt.Printf("Result: %d\n", a * b) 

    // Part 2
    oxygen := getEntry(lines, getMostCommon)
    co2 := getEntry(lines, getLeastCommon)
    fmt.Printf("oxygen: %s => %d\n", oxygen, binToDec(oxygen))
    fmt.Printf("co2: %s => %d\n", co2, binToDec(co2))
    fmt.Printf("Result: %d\n", binToDec(oxygen) * binToDec(co2)) 
}
