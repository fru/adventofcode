const fs = require("fs");
const filename = "input.txt";

const readFile = (fileName) => {
  const file = fs.readFileSync(fileName, "utf8");
  return file.split("\n");
};

const getMostCommon = (arr) => {
  const addVectors = (a, b) => {
    return b.map((x, i) => (a[i] += x));
  };
  const counts = Array.from({ length: arr[0].length }, () => 0);
  for (line of arr) {
    const a = Array.from(line, (c) => parseInt(c));
    addVectors(counts, a);
  }
  const half = arr.length / 2;
  const mostCommon = counts.map((x) => (x < half ? 0 : 1)).join("");
  return mostCommon;
};

const getLeastCommon = (arr) => {
  const mostCommon = getMostCommon(arr);
  const leastCommon = mostCommon.split("").map((x) => (x === "1" ? 0 : 1));
  return leastCommon.join("");
};

const binToDec = (bin) => {
  return parseInt(bin, 2);
};
const mostCommon = getMostCommon(readFile(filename));
const gamma = binToDec(mostCommon);
const epsilon = binToDec(getLeastCommon(readFile(filename)));

console.log(`Result: ${gamma * epsilon}`);

const getEntries = (entries, func, description) => {
  let pos = 0;
  while (entries.length > 1) {
    common = func(entries);
    entries = entries.filter((x) => x[pos] === common[pos]);
    pos++;
  }
  console.log(`${description}: ${entries[0]}`);
  return entries[0];
};

const oxygen = binToDec(
  getEntries(readFile(filename), getMostCommon, "mostCommon")
);
const co2 = binToDec(
  getEntries(readFile(filename), getLeastCommon, "mostCommon")
);

console.log(`Result 2: ${oxygen * co2}`);
