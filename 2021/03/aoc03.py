filename = 'input.txt'

def read_file(filename):
    with open(filename, 'r') as f:
        return f.read().splitlines()

def get_most_common(lst):
    counts = [0 for _ in range(0, len(lst[0]))]
    for line in lst:
        for i in range(0, len(line)):
            if line[i] == '1':
                counts[i] += 1

    half = len(lst) / 2
    most_common = ['0' if x < half else '1' for x in counts];
    return ''.join(most_common)

def get_least_common(lst):
    most_common = get_most_common(lst)
    return ''.join(['1' if x == '0' else '0' for x in most_common])

most_common = get_most_common(read_file(filename))
gamma = int(most_common, 2)
epsilon = int(get_least_common(read_file(filename)), 2)

print("Result:", gamma * epsilon)

# part 2
def get_entry(entries, func, description):
    pos = 1

    while len(entries) > 1:
        common = func(entries)
        # print(description, pos, common, len(entries))
        # for e in entries:
        #     print(f"{e[:pos]} {e[pos:]}")
        entries = [x for x in entries if x[pos-1] == common[pos-1]]
        pos += 1

    print (f"{description}: {entries[0]} => {int(entries[0], 2)}")
    return entries[0]

oxygen = int(get_entry(read_file(filename), get_most_common, 'most common'), 2)
co2 = int(get_entry(read_file(filename), get_least_common, 'least common'), 2)

print("Result 2:", oxygen * co2)
