package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
)

var versionSum int

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

const LEN_VERSION = 3
const LEN_TYPE_ID = 3
const LEN_LITERAL = 5
const TYPE_ID_LITERAL = 4

/** 
 * AAABBB
 * A - version
 * B - type id
 * if B == 4, then literal
 * => AAABBBCCCCCDDDDDEEEEE
 * C[0] = 1
 * D[0] = 1
 * E[0] = 0
 * if B != 4, then operator
 * => AAABBBI
 * if I == 0, then next 15 bits represent number of following bits
 * if I == 1, then nest 11 bits represent number of following sub-packets
 */
func strBinToInt2(bin *string, length int) int64 {
    toConvert := (*bin)[:length]
    if i, err := strconv.ParseInt(toConvert, 2, 64); err == nil {
        return i
    }
    fmt.Println("Error converting string to int")
    return 0
}

func strBinToInt(bin *string) int64 {
    return strBinToInt2(bin, len(*bin))
}

func getVersion(bin *string) int64 {
    result := strBinToInt2(bin, 3)
    fmt.Printf(" %s\tVersion: %d\n", (*bin)[:3], result)
    *bin = (*bin)[3:]
    versionSum += int(result)
    return result
}
func getTypeId(bin *string) int64 {
    result := strBinToInt2(bin, 3)
    fmt.Printf(" %s\tTypeId: %d\n", (*bin)[:3], result)
    *bin = (*bin)[3:]
    return result
}
func getLiteral(bin *string) int64 {
    tmp := ""
    var i int
    for i = 0; i < len(*bin); i += 5 {
        tmp += (*bin)[i+1:i+5]
        if (*bin)[i] == '0' {
            break
        }
    }
    literal := strBinToInt(&tmp)
    fmt.Printf(" %s\tLiteral: %d\n", (*bin)[:i+5], literal)
    *bin = (*bin)[i+5:]
    return literal
}

func getSubpacketLength(bin *string) (int64, bool) {
    lengthId := (*bin)[0:1]
    fmt.Printf(" %s\tLengthId: %s\n", lengthId, lengthId)
    *bin = (*bin)[1:]

    var lengthCount int
    packetsNotBits := lengthId == "1"

    if packetsNotBits {
        lengthCount = 11
        packetsNotBits = true
    } else {
        lengthCount = 15
    }

    length := strBinToInt2(bin, lengthCount)

    if packetsNotBits {
        fmt.Printf(" %s\tSub-Packets: %d\n", (*bin)[:lengthCount], length)
    } else {
        fmt.Printf(" %s\tSub-Bits: %d\n", (*bin)[:lengthCount], length)
    }
    *bin = (*bin)[lengthCount:]
    return length, packetsNotBits
}

func sum(in []int64) int64 {
    var sum int64
    for _, i := range in {
        sum += i
    }
    return sum
}

func mult(in []int64) int64 {
    var result int64 = 1
    for _, i := range in {
        result *= i
    }
    return result
}

func min(in []int64) int64 {
    min := in[0]
    for _, i := range in {
        if i < min {
            min = i
        }
    }
    return min
}

func max(in []int64) int64 {
    max := in[0]
    for _, i := range in {
        if i > max {
            max = i
        }
    }
    return max
}

func gt(in []int64) int64 {
    if in[0] > in[1] {
        return 1
    }
    return 0
}

func lt(in []int64) int64 {
    if in[0] < in[1] {
        return 1
    }
    return 0
}

func equal(in []int64) int64 {
    if in[0] == in[1] {
        return 1
    }
    return 0
}

func getOperator(id int64) (func([]int64) int64) {
    switch id {
    case 0:
        fmt.Println("Sum")
        return sum
    case 1:
        fmt.Println("Mult")
        return mult
    case 2:
        fmt.Println("Min")
        return min
    case 3:
        fmt.Println("Max")
        return max
    case 5:
        fmt.Println("Gt")
        return gt
    case 6:
        fmt.Println("Lt")
        return lt
    case 7:
        fmt.Println("Equal")
        return equal
    default:
        fmt.Println("Unknown operator")
        return nil
    }
}

func decodePacket(bin *string, packetDescription string) int64 {
    fmt.Printf("\n%s\n", packetDescription)
    getVersion(bin)
    typeId := getTypeId(bin)
    if typeId == TYPE_ID_LITERAL {
        return getLiteral(bin)
    } else {
        packets := make([]int64, 0)
        length, packetsNotBits := getSubpacketLength(bin)
        if packetsNotBits {
            for i := 0; int64(i) < length; i++ {
                description := fmt.Sprintf("%s > %d", packetDescription, i+1)
                packets = append(packets, decodePacket(bin, description))
            }
        } else {
            x := (*bin)[:length]
            packet := 1
            for len(x) > 0 {
                description := fmt.Sprintf("%s > %d", packetDescription, packet)
                packets = append(packets, decodePacket(&x, description))
                packet++
            }
            (*bin) = (*bin)[length:]
        }
        fmt.Println()
        result := getOperator(typeId)(packets)
        fmt.Printf(" = %d\n", result)
        for _, i := range packets {
            fmt.Printf(" %d", i)
        }
        return result
    }
}

func decode(bin string) int64 {
    x := &bin
    result := decodePacket(x, "Packet #")
    return result
}

func hexToBin(hex string) string {
    mapping := map[string]string{
        "0": "0000",
        "1": "0001",
        "2": "0010",
        "3": "0011",
        "4": "0100",
        "5": "0101",
        "6": "0110",
        "7": "0111",
        "8": "1000",
        "9": "1001",
        "A": "1010",
        "B": "1011",
        "C": "1100",
        "D": "1101",
        "E": "1110",
        "F": "1111",
    }
    result := ""
    for _, c := range hex {
        result += mapping[string(c)]
    }
    return result
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    bin := hexToBin(lines[0])
    fmt.Println(bin)
    result := decode(bin)
    fmt.Printf("\nVersion sum: %d\n", versionSum)
    fmt.Printf("Result: %d\n", result)
}
