package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseNumbers(line string) []int {
    splits := strings.Split(line, ",")
    numbers := make([]int, len(splits))
    for i := 0; i < len(splits); i++ {
        number, err := strconv.Atoi(splits[i])
        checkError(err)
        numbers[i] = number
    }
    return numbers
}

func minMax(list []int) (int, int) {
    min := list[0]
    max := min
    for i := 1; i < len(list); i++ {
        if list[i] < min {
            min = list[i]
        }
        if list[i] > max {
            max = list[i]
        }
    }
    return min, max
}

func fuel(moves int) int {
    return moves * (moves + 1) / 2
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    numbers := parseNumbers(lines[0])
    min, max := minMax(numbers)
    results := make([]int, max + 1)
    j := 0 
    for position := min; position <= max; position++ {
        result := 0
        for i := 0;  i < len(numbers); i++ {
            n := numbers[i]
            var moves int
            if n <= position {
                moves = position - n
            } else {
                moves = n - position
            }
            result += fuel(moves)
        }
        results[j] = result
        j++
    }
    min, _ = minMax(results)
    fmt.Println(min)
}
