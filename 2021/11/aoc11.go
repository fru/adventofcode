package main

import (
    "bufio"
    "fmt"
    "os"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) [][]int {
    matrix := make([][]int, len(lines))
    for i := 0; i < len(lines); i++ {
        matrix[i] = make([]int, len(lines[i]))
    }
    for i := 0; i < len(lines); i++ {
        for j := 0; j < len(lines[i]); j++ {
            matrix[i][j] = int(lines[i][j] - '0')
        }
    }
    return matrix
}

func countOneUp(raster [][]int) {
    for row := 0; row < len(raster); row++ {
        for col := 0; col < len(raster[row]); col++ {
            raster[row][col] += 1
        }
    }
}

func max(a int, b int) int {
    if a > b {
        return a
    }
    return b
}

func min(a int, b int) int {
    if a < b {
        return a
    }
    return b
}

func flash(raster [][]int, row int, col int) int {
    raster[row][col] = 0
    flashes := 1
    for i := max(row - 1, 0); i <= min(row + 1, len(raster) - 1); i++ {
        for j := max(col - 1, 0); j <= min(col + 1, len(raster[row]) - 1); j++ {
            old := raster[i][j]
            if old != 0 {
                raster[i][j] += 1
                if raster[i][j] > 9 {
                    flashes += flash(raster, i, j)
                }
            }
        }
    }
    return flashes
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    // Part 1
    raster := parseLines(lines)
    flashes := 0
    for step := 0; step < 100; step++ {
        countOneUp(raster)
        for row := 0; row < len(raster); row++ {
            for col := 0; col < len(raster[row]); col++ {
                if raster[row][col] > 9 {
                    flashes += flash(raster, row, col)
                }
            }
        }
    }
    fmt.Println(flashes)

    // Part 2
    raster = parseLines(lines)
    flashes = 0
    step := 0
    for flashes < 100 {
        step += 1
        countOneUp(raster)
        flashes = 0
        for row := 0; row < len(raster); row++ {
            for col := 0; col < len(raster[row]); col++ {
                if raster[row][col] > 9 {
                    flashes += flash(raster, row, col)
                }
            }
        }
    }
    fmt.Println(step)
}
