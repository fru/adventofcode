package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
    "unicode"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) [][]string {
    parsedLines := make([][]string, len(lines))
    for i := 0; i < len(lines); i++ {
        parsedLines[i] = strings.Split(lines[i], "-")
    }
    return parsedLines
}
func isLower(s string) bool {
    for _, r := range s {
        if !unicode.IsLower(r) && unicode.IsLetter(r) {
            return false
        }
    }
    return true
}

func findPath(caves string, currentCave string, visitedAgain bool, cavesMap map[string][]string, result* []string) {
    if currentCave == "end" {
        path := caves + "->" + currentCave
        *result = append(*result, path)
        // fmt.Printf("add path:~\n\t%s\n\t%s\n", path, result)
        return
    }
    if isLower(currentCave) && strings.Contains(caves, currentCave) {
        if visitedAgain || currentCave == "start" {
            return
        }
        visitedAgain = true
    }
    for i := 0; i < len(cavesMap[currentCave]); i++ {
        newPath := caves + "->" + currentCave
        nextCave := cavesMap[currentCave][i]
        // fmt.Println(newPath, nextCave)
        findPath(newPath, nextCave, visitedAgain, cavesMap, result)
    }
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    // Part 1
    connections := parseLines(lines)
    caves := make(map[string][]string)
    for i := 0; i < len(connections); i++ {
        caveA := connections[i][0]
        caveB := connections[i][1]
        caves[caveA] = append(caves[caveA], caveB)
        caves[caveB] = append(caves[caveB], caveA)
    }
    // fmt.Println(caves)
    results := make([]string, 0, len(caves))
    findPath("", "start", false, caves, &results)

    // for i := 0; i < len(results); i++ {
    //     fmt.Println(results[i])
    // }
    fmt.Println(len(results))

    // Part 2
}
