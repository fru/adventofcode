package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "regexp"
)

type Line struct {
    x1, y1, x2, y2 int
}

func (l *Line) Print() {
    fmt.Printf("%d,%d => %d,%d\n", l.x1, l.y1, l.x2, l.y2)
}

func (l *Line) IsSimple() bool {
    return l.x1 == l.x2 || l.y1 == l.y2
}

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) []Line {
    linesParsed := make([]Line, len(lines))
    re := regexp.MustCompile(`\d+`)
    for i := 0; i < len(lines); i++ {
        splits := re.FindAllString(lines[i], -1)
        result := make([]int, len(splits))
        for n := 0; n < len(splits); n++ {
            result[n], _ = strconv.Atoi(splits[n])
        }
        linesParsed[i] = Line{result[0], result[1], result[2], result[3]}
    }
    return linesParsed
}

func comp(d, a, b int) bool {
    if d >= 0 {
        return a <= b
    } else {
        return a >= b
    }
}

func main() {
    filename := "input.txt"
    boardSize := 1000
    lines := getLinesFromFile(filename)
    linesParsed := parseLines(lines)
    simpleLines := make([]Line, 0)
    for i := 0; i < len(linesParsed); i++ {
        if linesParsed[i].IsSimple() {
            simpleLines = append(simpleLines, linesParsed[i])
        }
    }
    board := make([][]int, boardSize)
    for i := 0; i < boardSize; i++ {
        board[i] = make([]int, boardSize)
    }
    result := 0
    linesToProcess := &linesParsed
    for i := 0; i < len(*linesToProcess); i++ {
        x1 := (*linesToProcess)[i].x1
        y1 := (*linesToProcess)[i].y1
        x2 := (*linesToProcess)[i].x2
        y2 := (*linesToProcess)[i].y2

        dX := 1
        dY := 1

        if x1 > x2 {
            dX = -1
        }

        if y1 > y2 {
            dY = -1
        }
        // x(1,3) | 1 <= 3 | 2 <= 3 | 3 <= 3 | 4 <= 3
        // y(1,3) | 3 <= 1 | 3 <= 2 | 3 <= 3 | 4 <= 3
        // x(3,1) | 3 >= 1 | 3 >= 2 | 3 >= 3 | 3 >= 4
        // ||     | true   | true   | true   | false
        //          
        // y1 = 1
        // x2 = 3
        // y2 = 3
        if (*linesToProcess)[i].IsSimple() {
            for x := x1; comp(dX, x, x2); x += dX {
                for y := y1; comp(dY, y, y2); y += dY {
                    board[y][x]++
                    if board[y][x] == 2 {
                        result++
                    }
                }
            }
        } else {
            d := x1 - x2
            if d < 0 {
                d = -d
            }
            for offset := 0; offset <= d; offset++ {
                x := x1 + dX * offset
                y := y1 + dY * offset
                board[y][x]++
                if board[y][x] == 2 {
                    result++
                }
            }
        }
    }

    for i := 0; i < len(board); i++ {
        for j := 0; j < len(board[i]); j++ {
            fmt.Printf("%d ", board[i][j])
        }
        fmt.Printf("\n")
    }
    fmt.Printf("Result: %d\n", result)
}
