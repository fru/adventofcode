package main

import (
    "fmt"
    "time"
)

func logDuration(name string, start time.Time) {
	fmt.Printf("%q took %s\n", name, time.Since(start))
}

type Position struct {
    x int
    y int
}

type Velocity struct {
    x int
    y int
}

func (p *Position) canReachTarget() (bool, bool) {
    targetX1 := 14
    targetX2 := 50
    targetY1 := -225
    targetY2 := -267
    if p.x <= targetX2 && p.x >= targetX1 && p.y <= targetY1 && p.y >= targetY2 {
        return true, true
    } else if p.x <= targetX2 && p.y >= targetY2 {
        return true, false
    }
    return false, false
}

func (p *Position) move(v *Velocity) {
    p.x += v.x
    p.y += v.y
    if v.x != 0 {
        v.x--
    }
    v.y--
}

// xt = x + x-1 + x-2 + ... + 0

func main() {
    defer logDuration("main", time.Now())
    p := Position{x: 0, y: 0}
    v := Velocity{x: 9, y: 1}
    heighestY := 0
    countSuccess := 0
    
    for y := -267; y <= 266; y++ {
        for x := 4; x <= 50; x++ {
            p = Position{x: 0, y: 0}
            v = Velocity{x: x, y: y}
            canReach, inTarget := p.canReachTarget()
            for canReach {
                p.move(&v)
                canReach, inTarget = p.canReachTarget()
                if inTarget {
                    fmt.Printf("Target reached speed x=%d and y=%d at (%d,%d)\n", x, y, p.x, p.y)
                    countSuccess++
                    break
                }
                if !canReach {
                    break
                }
                if p.y > heighestY {
                    heighestY = p.y
                }
            }
            if !canReach {
                continue
            }
        }
    }
    fmt.Printf("Heighest y=%d\n", heighestY)
    fmt.Printf("Count success=%d\n", countSuccess)
}
