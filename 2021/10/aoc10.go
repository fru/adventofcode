package main

import (
    "bufio"
    "fmt"
    "os"
    "sort"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func isOpening(c rune) bool {
    return c == '(' || c == '[' || c == '{' || c == '<'
}

func getMatch(c rune) rune {
    switch c {
    case ')':
        return '('
    case ']':
        return '['
    case '}':
        return '{'
    case '>':
        return '<'
    }
    return 0
}

func parseLine(line string, points map[rune]int) int {
    chars := []rune(line)
    heap := make([]rune, 0, len(chars))
    for i := 0; i < len(chars); i++ {
        c := chars[i]
        if isOpening(c) {
            heap = append(heap, c)
        } else {
            if len(heap) == 0 {
                return points[c]
            }
            last := heap[len(heap)-1]
            if last ==  getMatch(c) {
                heap = heap[:len(heap)-1]
            } else {
                return points[c]
            }
        }
    }
    return 0
}


func parseLines(lines []string, points map[rune]int) int {
    score := 0
    for i := 0; i < len(lines); i++ {
        score += parseLine(lines[i], points)
    }
    return score
}

func parseLine2(line string, points map[rune]int) int {
    chars := []rune(line)
    heap := make([]rune, 0, len(chars))
    for i := 0; i < len(chars); i++ {
        c := chars[i]
        if isOpening(c) {
            heap = append(heap, c)
        } else {
            if len(heap) == 0 {
                return 0
            }
            last := heap[len(heap)-1]
            if last ==  getMatch(c) {
                heap = heap[:len(heap)-1]
            } else {
                return 0
            }
        }
    }
    score := 0
    for i := 1; i <= len(heap); i++ {
        last := heap[len(heap)-i]
        score *= 5
        score += points[last]
    }
    return score
}

func parseLines2(lines []string, points map[rune]int) int {
    incompleteLinesScore := make([]int, 0, len(lines))
    for i := 0; i < len(lines); i++ {
        score := parseLine2(lines[i], points)
        if score != 0 {
            incompleteLinesScore = append(incompleteLinesScore, score)
        }
    }
    sort.Ints(incompleteLinesScore)
    return incompleteLinesScore[len(incompleteLinesScore)/2]
}

func main() {
    points := make(map[rune]int, 4)
    points[')'] = 3
    points[']'] = 57
    points['}'] = 1197
    points['>'] = 25137

    filename := "input.txt"
    lines := getLinesFromFile(filename)
    // Part 1
    result := parseLines(lines, points)
    fmt.Println(result)

    // Part 2
    points['('] = 1
    points['['] = 2
    points['{'] = 3
    points['<'] = 4
    result = parseLines2(lines, points)
    fmt.Println(result)
}
