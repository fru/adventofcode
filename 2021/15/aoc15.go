package main

import (
    "bufio"
    "fmt"
    "os"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) [][]int {
    result := make([][]int, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        result[i] = make([]int, len(line))
        for j := 0; j < len(line); j++ {
            result[i][j] = int(line[j] - '0')
        }
    }
    return result
}

func norm(x int) int {
    if x > 9 {
        return x - 10 + 1
    }
    return x
}

func bigCave(cave [][]int) [][]int {
    result := make([][]int, len(cave)*5)
    for i := 0; i < len(result); i++ {
        result[i] = make([]int, len(cave)*5)
    }
    for y := 0; y < 5; y++ {
        for x := 0; x < 5; x++ {
            for i := 0; i < len(cave); i++ {
                for j := 0; j < len(cave[0]); j++ {
                    result[i+(y*len(cave))][j+(x*len(cave[0]))] = norm(cave[i][j] + y + x)
                }
            }
        }
    }
    return result
}

type Position struct {
    x int
    y int
}

type PositionData struct {
    totalRisk int
    processed bool
}

func allProcessed(positions *map[Position]PositionData) bool {
    for _, v := range *positions {
        if v.processed {
            continue
        } else {
            return false
        }
    }
    return true
}

func replaceIfSmaller(positions *map[Position]PositionData, position Position, risk int) {
    oldRisk, ok := (*positions)[position]
    if !ok || oldRisk.totalRisk > risk {
        data := (*positions)[position]
        data.totalRisk = risk
        data.processed = false
        (*positions)[position] = data
    }
}

func getNextPositions(positions *map[Position]PositionData, cave *[][]int) {
    pos := make(map[Position]PositionData, len(*positions))
    for k, v := range *positions {
        if v.processed {
            continue
        }
        pos[k] = v
    }
    for k, v := range pos {
        if k.x > 1 && k.x < len((*cave)[0])-1 {
            newRisk := v.totalRisk + (*cave)[k.y][k.x-1]
            replaceIfSmaller(positions, Position{k.x-1, k.y}, newRisk)
        }
        if k.y > 1 && k.y < len(*cave)-1 {
            newRisk := v.totalRisk + (*cave)[k.y-1][k.x]
            replaceIfSmaller(positions, Position{k.x, k.y-1}, newRisk)
        }
        if k.x < len((*cave)[0])-1 {
            newRisk := v.totalRisk + (*cave)[k.y][k.x+1]
            replaceIfSmaller(positions, Position{k.x+1, k.y}, newRisk)
        }
        if k.y < len(*cave)-1 {
            newRisk := v.totalRisk + (*cave)[k.y+1][k.x]
            replaceIfSmaller(positions, Position{k.x, k.y+1}, newRisk)
        }
        data := (*positions)[k]
        data.processed = true
        (*positions)[k] = data
    }
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    cave := parseLines(lines)
    positions := make(map[Position]PositionData, 1)
    positions[Position{0, 0}] = PositionData{0, false}
    for !allProcessed(&positions) {
        getNextPositions(&positions, &cave)
    }
    size := len(cave)
    fmt.Println("Result (Part1):", positions[Position{size-1, size-1}].totalRisk)

    bCave := bigCave(cave)

    positions = make(map[Position]PositionData, 1)
    positions[Position{0, 0}] = PositionData{0, false}
    
    for !allProcessed(&positions) {
        getNextPositions(&positions, &bCave)
    }
    size = len(bCave)
    fmt.Println("Result (Part2):", positions[Position{size-1, size-1}].totalRisk)
}
