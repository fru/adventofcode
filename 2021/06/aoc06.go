package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseNumbers(line string) []int {
    splits := strings.Split(line, ",")
    numbers := make([]int, len(splits))
    for i := 0; i < len(splits); i++ {
        number, err := strconv.Atoi(splits[i])
        checkError(err)
        numbers[i] = number
    }
    return numbers
}

type CacheKey struct {
    initDays int
    daysLeft int
}

func calculateNumberOfFish(initDays int, daysLeft int, cache map[CacheKey]int64) int64 {
    key := CacheKey{initDays, daysLeft}
    if val, ok := cache[key]; ok {
        return val
    }
    if initDays >=  daysLeft {
        cache[key] = 0
        return 0
    }
    var count int64
    count = 0
    for daysLeft > 0 {
        if initDays > 0 {
            initDays--
            daysLeft--
        } else {
            initDays = 6
            daysLeft--
            count += 1 + calculateNumberOfFish(8, daysLeft, cache)
        }
    }
    cache[key] = count
    return count
}

func main() {
    filename := "input.txt"
    daysLeft := 256 
    if len(os.Args) > 1 {
        filename = os.Args[1]
    }
    if len(os.Args) > 2 {
        daysLeft, _ = strconv.Atoi(os.Args[2])
    }
    lines := getLinesFromFile(filename)
    numbers := parseNumbers(lines[0])

    var count int64
    count = int64(len(numbers))
    cache := make(map[CacheKey]int64)
    for i := 0; i < len(numbers); i++ {
        count += calculateNumberOfFish(numbers[i], daysLeft, cache)
    }
    fmt.Println(count)

}

