const fs = require("fs");

class Board {
  constructor(input) {
    this.board = input;
    this.matches = [];
    this.finished = false;
    this.initMatches();
  }

  initMatches() {
    this.matches = Array.from({ length: 5 }, () => 0).map((_) =>
      Array.from({ length: 5 }, () => false)
    );
  }

  handleDraw(draw) {
    for (let [iRow, row] of this.board.entries()) {
      for (let [iCol, value] of row.entries()) {
        if (value === draw) {
          this.matches[iRow][iCol] = true;
        }
      }
    }
    return this.checkWin(draw);
  }

  checkWin(lastDraw) {
    for (let [iRow, row] of this.matches.entries()) {
      let column = [];
      for (let j = 0; j < 5; j++) {
        column[j] = this.matches[j][iRow];
      }
      if (column.every((x) => x === true) || row.every((x) => x === true)) {
        this.finished = true;
        return this.calculateScore(lastDraw);
      }
    }
    return 0;
  }

  calculateScore(lastDraw) {
    let score = 0;
    for (let [iRow, row] of this.board.entries()) {
      for (let [iCol, value] of row.entries()) {
        if (!this.matches[iRow][iCol]) {
          score += value;
        }
      }
    }
    return score * lastDraw;
  }
}

class Game {
  constructor(filename) {
    this.boards = [];
    this.draws = [];
    this.parseInput(filename);
  }

  readFile(fileName) {
    const file = fs.readFileSync(fileName, "utf8");
    return file.split("\n");
  }

  parseInput(filename) {
    const data = this.readFile(filename);
    this.draws = data
      .shift()
      .split(",")
      .map((x) => parseInt(x));

    let board = [];
    for (let line of data) {
      if (line.length < 5 && board.length > 0) {
        this.boards.push(new Board(board));
        board = [];
        continue;
      }
      if (line.length > 5) {
        board.push(
          line
            .trim()
            .split(/\s+/)
            .map((x) => parseInt(x))
        );
      }
    }
    if (board.length > 0) {
      this.boards.push(new Board(board));
    }
  }

  playPart1() {
    let score = 0;
    while (score == 0 && this.boards.length > 0) {
      let draw = this.draws.shift();
      for (let board of this.boards) {
        score += board.handleDraw(draw);
      }
      if (score > 0) {
        console.log(`Result: ${score}`);
        return score;
      }
    }
  }

  playPart2() {
    while (this.boards.length > 1) {
      let draw = this.draws.shift();
      for (let board of this.boards) {
        board.handleDraw(draw);
      }
      this.boards = this.boards.filter((x) => !x.finished);
    }
    return this.playPart1();
  }
}

const filename = "input.txt";

const game = new Game(filename);
game.playPart1();

const game2 = new Game(filename);
game2.playPart2();
