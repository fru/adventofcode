import re


class Board:
    def __init__(self, board_data):
        self.board = board_data
        self.matches = [[False for _ in range(5)] for _ in range(5)] 
        self.finished = False

    def __str__(self):
        return '\n'.join([' '.join([str(x).rjust(2) for x in row]) for row in self.board])

    def __repr__(self):
        return f"<Board\n{self.__str__()}\n>"

    def printMatches(self):
        return '\n'.join([' '.join([str(x) for x in row]) for row in self.matches])

    def handle_draw(self, draw):
        for i in range(5):
            for j in range(5):
                if self.board[i][j] == draw:
                    self.matches[i][j] = True
        return self.check_win(draw)

    def check_win(self, last_draw):
        for i in range(5):
            column = [self.matches[j][i] for j in range(5)]
            if all(self.matches[i]) or all(column):
                self.finished = True
                return self.calculate_score(last_draw)
        return 0

    def calculate_score(self, last_draw):
        score = 0
        for i in range(5):
            for j in range(5):
                if not self.matches[i][j]:
                    score += self.board[i][j]
        return score * last_draw

class Game:
    def __init__(self):
        filename = 'input.txt'
        self.draws, self.boards = self.parse_input(self.read_file(filename))

    def read_file(self, filename):
        with open(filename, 'r') as f:
            return f.read().splitlines()

    def parse_input(self, input_data):
        draws = [int(x) for x in input_data[0].split(',')]
        board = []
        boards = []
        for line in input_data[1:]:
            if len(line) < 5 and len(board) > 0:
                boards.append(Board(board))
                board = []
                continue
            if len(line) > 5:
                splits = re.split(' +', line.strip())
                board.append([int(x) for x in splits])
        boards.append(Board(board))
        return draws, boards

    def play_game_part1(self):
        score = 0
        while score == 0:
            draw = self.draws.pop(0)
            for board in self.boards:
                score = board.handle_draw(draw)
                if score > 0:
                    print(f'Result: {score}')
                    return score

    def play_game_part2(self):
        while len(self.boards) > 1:
            draw = self.draws.pop(0)
            for board in self.boards:
                board.handle_draw(draw)
            self.boards = [board for board in self.boards if not board.finished]
        return self.play_game_part1()


if __name__ == '__main__':
    game = Game()
    game.play_game_part1()

    game = Game()
    game.play_game_part2()

