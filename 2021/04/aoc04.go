package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
    "regexp"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

type Board struct {
    Board [][]int
    Matches [][]bool
    IsFinished bool
}

func (b *Board) InitMatches() {
    b.Matches = make([][]bool, 5)
    for i := 0; i < 5; i++ {
        b.Matches[i] = make([]bool, 5)
    }
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            b.Matches[i][j] = false
        }
    }
}

func (b *Board) CalculateScore() int {
    score := 0
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            if b.Matches[i][j] == false {
                score += b.Board[i][j]
            }
        }
    }
    return score
}

func (b *Board) CheckWin() int {
    for i := 0; i < 5; i++ {
        rowTrueCount := 0
        columnTrueCount := 0
        for j := 0; j < 5; j++ {
            if b.Matches[i][j] == true {
                rowTrueCount++
            }
            if b.Matches[j][i] == true {
                columnTrueCount++
            }
        }
        if rowTrueCount == 5 ||  columnTrueCount == 5 {
            b.IsFinished = true
            return b.CalculateScore()
        }
    }
    return 0
}

func (b *Board) HandleDraw(draw int) int {
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            if b.Board[i][j] == draw {
                b.Matches[i][j] = true
            }
        }
    }
    return b.CheckWin() * draw
}

func (b *Board) Print() {
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            fmt.Printf("%2v ", b.Board[i][j])
        }
        fmt.Println()
    }
    fmt.Println()
}

func (b *Board) PrintMatches() {
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            fmt.Printf("%2v ", b.Matches[i][j])
        }
        fmt.Println()
    }
    fmt.Println()
}

type Game struct {
    Boards []Board
    Draws []int
}

func (game *Game) GetLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func (game *Game) ParseDraws(line string) {
    drawsStr := strings.Split(line, ",")
    draws := make([]int, len(drawsStr))
    for i, drawStr := range drawsStr {
        draw, err := strconv.Atoi(drawStr)
        checkError(err)
        draws[i] = draw
    }
    game.Draws = draws
}

func (game *Game) ParseBoards(lines []string) {
    board := Board{}
    board.InitMatches()
    for _, line := range lines {
        if len(line) == 0 {
            if len(board.Board) > 0 {
                game.Boards = append(game.Boards, board)
                board = Board{}
                board.InitMatches()
            }
            continue
        }
        numbers := make([]int, 5)
        spaces := regexp.MustCompile(`\s+`)
        trimmed := strings.TrimPrefix(line, " ")
        nStrs := spaces.Split(trimmed, -1)
        for i, nStr := range nStrs {
            n, err := strconv.Atoi(nStr)
            checkError(err)
            numbers[i] = n
        }
        board.Board = append(board.Board, numbers)
    }
}

func (game *Game) ParseFile(filename string) {
    lines := game.GetLinesFromFile(filename)
    game.ParseDraws(lines[0])
    game.ParseBoards(lines[1:])
}

func (game *Game) PlayPart1() {
    score := 0
    draw := 0
    for score == 0 {
        draw, game.Draws = game.Draws[0], game.Draws[1:]
        for _, board := range game.Boards {
            score = board.HandleDraw(draw)
            if score > 0 {
                fmt.Printf("Result(Part 1): %v\n", score)
                return
            }
        }
    }
}

func (game *Game) PlayPart2() {
    draw := 0
    for len(game.Boards) > 1 {
        draw, game.Draws = game.Draws[0], game.Draws[1:]
        for i := 0; i < len(game.Boards); i++ {
            game.Boards[i].HandleDraw(draw)
        }
        for i := 0; i < len(game.Boards); i++ {
            if game.Boards[i].IsFinished == true {
                game.Boards = append(game.Boards[:i], game.Boards[i+1:]...)
                i--
            }
        }
    }
    game.PlayPart1()
}

func main() {
    game := Game{}
    game.ParseFile("input.txt")
    game.PlayPart1()

    game2 := Game{}
    game2.ParseFile("input.txt")
    game2.PlayPart2()

}
