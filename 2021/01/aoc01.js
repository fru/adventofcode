const fs = require("fs");

const readFile = (fileName) => {
  const file = fs.readFileSync(fileName, "utf8");
  return file.split("\n");
};

numbers = readFile("input.txt").map((x) => parseInt(x));

increases = numbers.filter((x, i) => {
  if (i === numbers.length - 1) {
    return;
  }
  return x < numbers[i + 1];
}).length;

increasesWithWindow = numbers.filter((x, i) => {
  if (i === numbers.length - 1 || i < 3) {
    return;
  }
  return (
    numbers[i - 3] + numbers[i - 2] + numbers[i - 1] <
    numbers[i - 2] + numbers[i - 1] + x
  );
}).length;

console.log(`Number of increases: ${increases}`);
console.log(`Number of increases with sliding window: ${increasesWithWindow}`);
