with open('input.txt', 'r') as f:
    input_data = f.readlines()

numbers  = [int(x) for x in input_data]
increases = 0
increasesWindow = 0
window = [0, 0, 0]
count = 0
for i in range(1, len(numbers)):
    if numbers[i] > numbers[i-1]:
        increases += 1
    if count < 2:
        window[count] = numbers[i]
        count += 1
        continue

    if sum(window) < sum(window[1:]) + numbers[i]:
        increasesWindow += 1

    window = [window[1], window[2], numbers[i]]

print(f'Number of increases: {increases}')
print(f'Number of increases with sliding window: {increasesWindow}')
