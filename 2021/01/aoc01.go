package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}

func isIncreased(numbers *[3]int, newNumber int) bool {
	sum1 := 0
	sum2 := 0
	for _, num := range numbers {
		sum1 += num
	}
	sum2 = numbers[1] + numbers[2] + newNumber

	numbers[0] = numbers[1]
	numbers[1] = numbers[2]
	numbers[2] = newNumber
	return sum1 < sum2
}

func getNumber(line string) int {
	number, err := strconv.Atoi(line)
	checkError(err)
	return number
}

func main() {
	file, err := os.Open("input.txt")
	checkError(err)
	defer file.Close()

	increases := 0
	increasesWithWindow := 0
	startCount := 0
	previous := 99999

	var buffer [3]int
	var currentNumber int

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		currentNumber = getNumber(scanner.Text())

		if currentNumber > previous {
			increases++
		}
		previous = currentNumber

		if startCount < 3 {
			buffer[startCount] = currentNumber
			startCount++
			continue
		}

		if isIncreased(&buffer, currentNumber) {
			increasesWithWindow++
		}
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
	fmt.Printf("Number of increases: %d \n", increases)
	fmt.Printf("Number of increases with sliding window: %d \n", increasesWithWindow)
}
