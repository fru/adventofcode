def read_file(filename):
    with open(filename, 'r') as f:
        return f.read().splitlines()

distance = 0
depth = 0
aim = 0

for line in read_file('input.txt'):
    (direction, value) = line.split(' ')
    value = int(value)
    if direction == 'forward':
        distance += value
        depth += aim * value
    elif direction == 'up':
        aim -= value
    elif direction == 'down':
        aim += value

print(f'Distance: {distance}')
print(f'Depth: {depth}')
print(f'Total: {distance * depth}')
