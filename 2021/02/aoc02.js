const fs = require("fs");

const readFile = (fileName) => {
  const file = fs.readFileSync(fileName, "utf8");
  return file.split("\n");
};

input = readFile("input.txt")
  .map((x) => x.split(" "))
  .map(([direction, value]) => ({ direction, value: parseInt(value) }));

let depth = 0;
let distance = 0;
let aim = 0;

for (const { direction, value } of input) {
  switch (direction.toLowerCase()) {
    case "forward":
      distance += value;
      depth += aim * value;
      break;
    case "up":
      aim -= value;
      break;
    case "down":
      aim += value;
      break;
  }
}

console.log(`Distance: ${distance}`);
console.log(`Depth: ${depth}`);
console.log(`Total: ${distance * depth}`);
