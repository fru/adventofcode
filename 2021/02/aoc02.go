package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getDirectionAndValue(line string) (string, int) {
    splits := strings.Split(line, " ")
    value, err := strconv.Atoi(splits[1])
    checkError(err)
    return splits[0], value
}

func main() {
    file, err := os.Open("input.txt")
    checkError(err)
    defer file.Close()

    distance := 0
    depth := 0
    aim := 0

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()
        direction, value := getDirectionAndValue(line)
        switch direction {
        case "forward":
            distance += value
            depth += aim * value
        case "up":
            aim -= value
        case "down":
            aim += value
        }
    }

    fmt.Printf("Distance: %d\n", distance)
    fmt.Printf("Depth: %d\n", depth)
    fmt.Printf("Total: %d\n", distance * depth)
}
