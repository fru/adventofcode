package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
    "sync"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) map[string]string {
    mapping := make(map[string]string, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        parts := strings.Split(line, " -> ")
        mapping[parts[0]] = parts[1]
    }
    return mapping
}

func parseLinesRune(lines []string) map[string]rune {
    mapping := make(map[string]rune, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        parts := strings.Split(line, " -> ")
        mapping[parts[0]] = rune(parts[1][0])
    }
    return mapping
}

func substitude2(template *[]rune, mapping *map[string]rune) {
    for i := 0; i < len(*template)-1; i++ {
        ab := string((*template)[i]) + string((*template)[i+1])
        *template = append((*template), 0)
        copy((*template)[i+1:], (*template)[i:])
        (*template)[i] = (*mapping)[ab]
        i++
    }
}

type runeCount struct {
    sync.RWMutex
    count map[rune]int64
}

/*
 cache[KEY] = []string
 cache[NN][0] = NCN
 cache[NN][1] = NBCCN
 cache[NN][2] = NBBBCNCCN
*/

type Cache struct {
    cache map[string][]string
}

func (c *Cache) get(key string, step int) (string, error) {
    result, ok := c.cache[key]
    if !ok {
        return "", fmt.Errorf("Key %s not found", key)
    }
    if len(result) <= step-1 {
        return "", fmt.Errorf("Step %d not found", step)
    }
    return result[step-1], nil
}

func (c *Cache) getLast(key string) string {
    result := c.cache[key]
    return result[len(result)-1]
}

func (c *Cache) set(key string, step int, value string) {
    c.cache[key] = append(c.cache[key], value)
}

func (c *Cache) print() {
    for k, v := range c.cache {
        fmt.Println(k)
        for i := 0; i < len(v); i++ {
            fmt.Printf("%d: %s\n", i, v[i])
        }
    }
}

func buildCache(mapping *map[string]string) *Cache {
    cache := Cache{cache: make(map[string][]string)}
    for k, v := range *mapping {
        cache.cache[k] = make([]string, 1, 5)
        cache.cache[k][0] = string(k[0]) + v + string(k[1])
    }
    for step := 1; step < 5; step++ {
        for k, v := range cache.cache {
            result := string(v[step-1][0])
            for i := 0; i < len(v[step-1])-1; i++ {
                ab := v[step-1][i:i+2]
                r, err := cache.get(ab, step)
                checkError(err)
                result += r[1:]
            }
            // fmt.Printf("key: %s | step: %d | len: %d\n", k, step, len(result))
            cache.set(k, step+1, result)
        }
    }
    fmt.Println("Cache built")
    return &cache
}

func calculateStep(step int) int {
    if step == 2 {
        return 4
    }
    if step == 1 {
        return 5
    }
    return 0
}

func substitude(template string, step int, cache *Cache, count *map[rune]int64, counts *map[string]map[rune]int64) {
    if (step == 0) {
        for i := 0; i < len(template)-1; i++ {
            ab := template[i:i+2]
            c := (*counts)[ab]
            for k, v := range c {
                (*count)[k] += v
            }
        }
        return
    }
    // fmt.Printf("Substitude: %s | len: %d | step: %d\n", template[:len(template)%20], len(template), step)
    for i := 0; i < len(template)-1; i++ {
        ab := template[i:i+2]
        if step == 2 {
            fmt.Println(ab)    
        }
        r, err := cache.get(ab, calculateStep(step))
        checkError(err)
        substitude(r, step-1, cache, count, counts)
    }
}


func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    template := lines[0]
    mapping := parseLines(lines[2:])
    cache := buildCache(&mapping)
    // cache.print()
    count := make(map[rune]int64)
    counts := make(map[string]map[rune]int64)
    for k, v := range cache.cache {
        counts[k] = make(map[rune]int64)
        for i := 1; i < len(v[4]); i++ {
            counts[k][rune(v[4][i])]++
        }
    }
    fmt.Println("Counts built")
    count[rune(template[0])]++
    substitude(template, 2, cache, &count, &counts)
    min := 'N'
    max := 'N'
    for k, v := range count {
        if v > count[max] {
            max = k
        }
        if v < count[min] {
            min = k
        }
        fmt.Printf("%s: %d\n", string(k), v)
    }
    fmt.Println("Min: ", string(min), count[min])
    fmt.Println("Max: ", string(max), count[max])
    fmt.Println("Result: ", count[max] - count[min])
}
