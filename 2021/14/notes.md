NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C

NNCB
NN NC CB
 C  B  H

NCN    NBC     CHB
NC CN NB BC CH HB
 B  C  C  B  B  C

NBC CCN NCB BBC CBH HCB

B C N H 

cache[0] = 1. Step
cache[1] = 2. Step => 1. Step & 1. Step
cache[2] = 4. Step => 2. Step & 2. Step
cache[3] = 8. Step => 4. Step & 4. Step
cache[4] = 16. Step => 8. Step & 8. Step
cache[5] = 32. Step => 16. Step & 16. Step

10 => 8. Step & 2. Step
40 => 32. Step & 8. Step

0  => 2
1  => 3
2  => 5
3  => 9
4  => 17
5  => 33
6  => 65
7  => 131
8  => 263
9  => 527
10 => 1055
