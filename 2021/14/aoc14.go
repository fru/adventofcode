package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
    "sync"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) map[string]string {
    mapping := make(map[string]string, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        parts := strings.Split(line, " -> ")
        mapping[parts[0]] = parts[1]
    }
    return mapping
}

func parseLinesRune(lines []string) map[string]rune {
    mapping := make(map[string]rune, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        parts := strings.Split(line, " -> ")
        mapping[parts[0]] = rune(parts[1][0])
    }
    return mapping
}

func substitude(template string, mapping map[string]string) string {
    result := string(template[0]) 
    for i := 0; i < len(template)-1; i++ {
        a, b := string(template[i]), string(template[i+1])
        result += mapping[a+b] + b
    }
    return result
}

func substitude2(template *[]rune, mapping *map[string]rune) {
    for i := 0; i < len(*template)-1; i++ {
        ab := string((*template)[i]) + string((*template)[i+1])
        *template = append((*template), 0)
        copy((*template)[i+1:], (*template)[i:])
        (*template)[i] = (*mapping)[ab]
        i++
    }
}
type runeCount struct {
    sync.RWMutex
    count map[rune]int64
}

func sub(a rune, b rune, steps int, mapping *map[string]rune, count *runeCount, g *sync.WaitGroup) {
    key := string(a) + string(b)
    if steps == 1 {
        count.Lock()
        count.count[b]++
        count.count[(*mapping)[key]]++
        count.Unlock()
        g.Done()
        return
    }
    defer g.Done()
    if steps > 5 {
        t := make([]rune, len(key))
        t[0] = a
        t[1] = b
        for i := 0; i < 5; i++ {
            substitude2(&t, mapping)
            steps--
        }
        fmt.Println(len(t), string(t))
        for i := 0; i < len(t)-1; i++ {
            g.Add(1)
            sub(t[i], t[i+1], 25, mapping, count, g)
        }
        return
    }
    g.Add(1)
    sub(a, (*mapping)[key], steps-1, mapping, count, g)
    g.Add(1)
    sub((*mapping)[key], b, steps-1, mapping, count, g)
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    template := lines[0]
    templateRune := []rune(template)
    mappingRune := parseLinesRune(lines[2:])
    g := sync.WaitGroup{}
    count := runeCount{count: make(map[rune]int64)}
    count.count[templateRune[0]] = 1
    for i := 0; i < len(templateRune)-1; i++ {
        g.Add(1)
        go sub(templateRune[i], templateRune[i+1], 10, &mappingRune, &count, &g)
    }
    g.Wait()
    min := 'B'
    max := 'B'
    for k, v := range count.count {
        if v > count.count[max] {
            max = k
        }
        if v < count.count[min] {
            min = k
        }
    }
    fmt.Println("Min: ", string(min), count.count[min])
    fmt.Println("Max: ", string(max), count.count[max])
    fmt.Println("Result: ", count.count[max] - count.count[min])
}
