package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) map[string]string {
    mapping := make(map[string]string, len(lines))
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        parts := strings.Split(line, " -> ")
        mapping[parts[0]] = parts[1]
    }
    return mapping
}

/**
 * Solution inspired by:
 * https://www.youtube.com/watch?v=7zvA-o47Uo0
 */
func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    template := lines[0]
    mapping := parseLines(lines[2:])
    count := make(map[string]int64)
    for i := 0; i < len(template); i++ {
        count[string(template[i])]++
    }
    counter := make(map[string]int64)
    for i := 0; i < len(template)-1; i++ {
        counter[template[i:i+2]]++
    }
    for i := 0; i < 40; i++ {
        tmpCounter := make(map[string]int64)
        for k, v := range counter {
            count[mapping[k]] += v
            tmpCounter[k[:1]+mapping[k]] += v
            tmpCounter[mapping[k]+k[1:]] += v
        }
        counter = tmpCounter
    }
    min := template[:1]
    max := template[:1]
    for k, v := range count {
        if v > count[max] {
            max = k
        }
        if v < count[min] {
            min = k
        }
        fmt.Printf("%s: %d\n", string(k), v)
    }
    fmt.Println("Min: ", string(min), count[min])
    fmt.Println("Max: ", string(max), count[max])
    fmt.Println("Result: ", count[max] - count[min])
}
