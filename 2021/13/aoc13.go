package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

type Fold struct {
    axis rune
    value int
}

func (f *Fold) String() string {
    return fmt.Sprintf("%c: %d", f.axis, f.value)
}

func parseLines(lines []string, sizeX int, sizeY int) ([][]bool, []Fold) {
    result := make([][]bool, sizeY)
    folds := make([]Fold, 0, 5)
    for i := 0; i < len(result); i++ {
        result[i] = make([]bool, sizeX)
    }
    for i := 0; i < len(lines); i++ {
        line := lines[i]
        if len(line) > 0 && len(line) < 12 {
            splits  := strings.Split(line, ",")
            x, _ := strconv.Atoi(splits[0])
            y, _ := strconv.Atoi(splits[1])
            result[y][x] = true
        } else if len(line) > 11 {
            axis := rune(line[11])
            value, _ := strconv.Atoi(line[13:])
            folds = append(folds, Fold{axis, value})
        }
    }
    return result, folds
}

func fold(matrix [][]bool, fold Fold) [][]bool {
    sizeX, sizeY := len(matrix[0]), len(matrix)
    if fold.axis == 'x' {
        sizeX = fold.value
    } else {
        sizeY = fold.value
    }
    result := make([][]bool, sizeY)
    for i := 0; i < len(result); i++ {
        result[i] = make([]bool, sizeX)
    }
    for y := 0; y < len(matrix); y++ {
        for x := 0; x < len(matrix[y]); x++ {
            value := matrix[y][x]
            if value {
                yNew := y
                xNew := x
                if y == sizeY {
                    continue
                }
                if y > sizeY {
                    yNew = sizeY - ((y - 1) % sizeY) - 1
                }
                if x == sizeX {
                    continue
                }
                if x > sizeX {
                    xNew = sizeX - ((x - 1) % sizeX) - 1
                }
                result[yNew][xNew] = value
            }
        }
    }
    return result
}

func Print(matrix [][]bool) {
    for y := 0; y < len(matrix); y++ {
        for x := 0; x < len(matrix[y]); x++ {
            if matrix[y][x] {
                fmt.Print("#")
            } else {
                fmt.Print(" ")
            }
        }
        fmt.Println()
    }
}

func count(matrix [][]bool) int {
    count := 0
    for y := 0; y < len(matrix); y++ {
        for x := 0; x < len(matrix[y]); x++ {
            if matrix[y][x] {
                count++
            }
        }
    }
    return count
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    matrix, folds := parseLines(lines, 2*655+1, 2*447+1)
    for i := 0; i < len(folds); i++ {
        matrix = fold(matrix, folds[i])
        if i == 0 {
            fmt.Printf("Part1: %d\n", count(matrix))
        }
    }
    fmt.Println("Part2:")
    Print(matrix)


}
