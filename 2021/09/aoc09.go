package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
    "strconv"
    "sort"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func parseLines(lines []string) [][]int {
    rowSize := len(lines) + 2
    colSize := len(lines[0]) + 2
    data := make([][]int, rowSize)
    data[0] = make([]int, colSize)
    data[rowSize - 1] = make([]int, colSize)
    for i := 0; i < colSize; i++ {
        data[0][i] = 9
        data[len(data) - 1][i] = 9
    }
    for i := 0; i < len(lines); i++ {
        data[i+1] = make([]int, colSize)
        data[i+1][0] = 9
        data[i+1][colSize - 1] = 9
        line := strings.Split(lines[i], "")
        for j := 0; j < len(line); j++ {
            data[i+1][j+1], _ = strconv.Atoi(line[j])
        }
    }
    return data
}

func getLowSportScore(data [][]int, row int, col int) int {
    spot := data[row][col]
    up := data[row - 1][col]
    down := data[row + 1][col]
    left := data[row][col - 1]
    right := data[row][col + 1]
    if right > spot && up > spot && down > spot && left > spot {
        return spot + 1
    }
    return 0
}

type Direction uint8


func calculateBasinScore(data [][]int, row int, col int) int {
    if (data[row][col] == 9) {
        return 0
    }
    score := 1
    data[row][col] = 9
    score += calculateBasinScore(data, row + 1, col)
    score += calculateBasinScore(data, row - 1, col)
    score += calculateBasinScore(data, row, col + 1)
    score += calculateBasinScore(data, row, col - 1)

    return score
}

func print(data [][]int) {
    for i := 0; i < len(data); i++ {
        for j := 0; j < len(data[i]); j++ {
            fmt.Printf("%d ", data[i][j])
        }
        fmt.Println()
    }
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    data := parseLines(lines)
    // Part 1
    sum := 0
    for row := 1; row < len(data) - 1; row++ {
        for col := 1; col < len(data[row]) - 1; col++ {
            sum += getLowSportScore(data, row, col)
        }
    }
    fmt.Println(sum)

    // Part 2
    basins := make([]int, 0)
    for row := 1; row < len(data) - 1; row++ {
        for col := 1; col < len(data[row]) - 1; col++ {
            if data[row][col] != 9 {
                basin := calculateBasinScore(data, row, col)
                if basin > 0 {
                    basins = append(basins, basin)
                }
            }
        }
    }
    sort.Ints(basins)
    biggest := basins[len(basins) - 3:]
    sum = 1
    for i := 0; i < len(biggest); i++ {
        sum *= biggest[i]
    }
    fmt.Println(sum)

}
