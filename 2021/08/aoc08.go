package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
)

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func getLinesFromFile(filename string) []string {
    file, err := os.Open(filename)
    checkError(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

type Data struct {
    input []string
    output []string
}

func parseLines(lines []string) []Data {
    data := make([]Data, len(lines))
    for i, line := range lines {
        rawInputs := strings.Split(line, " | ")
        input := strings.Split(rawInputs[0], " ")
        output := strings.Split(rawInputs[1], " ")
        d := Data{input: input, output: output}
        data[i] = d
    }
    return data
}

type Bits uint8

func getBitMask(s string) Bits {
    b := Bits(0)
    x := "abcdefg"
    for c := 0; c < len(x); c++ {
        if strings.Contains(s, string(x[c])) {
            b |= 1
        }
        b <<= 1
    }
    b >>= 1
    b |= Bits(1) << 7
    return b
}

func addToStores(n int, b Bits, store map[int]Bits, store2 map[Bits]int) {
    store[n] = b
    store2[b] = n
}

func processInput(s string, store map[int]Bits, store2 map[Bits]int) {
    b := getBitMask(s)
    if _, ok := store2[b]; ok {
        return
    }

    l := len(s)
    switch {
    case l == 2:
        addToStores(1, b, store, store2)
        return
    case l == 4:
        addToStores(4, b, store, store2)
        return
    case l == 3:
        addToStores(7, b, store, store2)
        return
    case l == 7:
        addToStores(8, b, store, store2)
        return
    }
    if b4, ok := store[4]; ok {
        if l == 6 && b4 & b == b4 {
            addToStores(9, b, store, store2)
            return
        }
    } else {
        return
    }
    if b7, ok := store[7]; ok {
        if b7 & b == b7 {
            if l == 6 {
                addToStores(0, b, store, store2)
                return
            }
            if l == 5 {
                addToStores(3, b, store, store2)
                return
            }
        }
    } else {
        return
    }
    if l == 6 {
        addToStores(6, b, store, store2)
        return
    }
    if b4, ok := store[4]; ok {
        if b3, ok := store[3]; ok {
            help := b4 & ^b3
            if help & b == help {
                addToStores(5, b, store, store2)
            } else {
                addToStores(2, b, store, store2)
            }
            return
        }
    }
}

func main() {
    filename := "input.txt"
    lines := getLinesFromFile(filename)
    data := parseLines(lines)
    count := 0
    var store map[int]Bits
    var store2 map[Bits]int
    for i := 0; i < len(data); i++ {
        store = make(map[int]Bits, 10)
        store2 = make(map[Bits]int, 10)
        inputs := data[i].input
        for len(store) < 10 {
            for j := 0; j < len(inputs); j++ {
                input := inputs[j]
                processInput(input, store, store2)
            }
        }

        outputs := data[i].output
        multi := 1000
        for j := 0; j < len(outputs); j++ {
            output := outputs[j]
            b := getBitMask(output)
            if n, ok := store2[b]; ok {
                count += n * multi
            }
            multi /= 10
        }
    }
    fmt.Println(count)
}
